// In App.js in a new project

import * as React from 'react';
import { View, Text, Button, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from 'native-base';


function HomeScreen({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button 
        title="Go to Detail"
        onPress={() => navigation.navigate('Detail', {
          id: 1,
          name: 'Dara'
        })}
      />
    </View>
  );
}

function DetailScreen({navigation, route}) {
  const {id, name} = route.params
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Detail Screen</Text>
      <Text>ID : {JSON.stringify(id)}</Text>
      <Text>Hello : {JSON.stringify(name)}</Text>
      <Button
        title="Go to Details... again"
        onPress={() => navigation.navigate('Detail')}
      />
       <Button
        title="Go to Details by Push... again"
        onPress={() => navigation.push('Detail')}
      />
      <Button
        title="Go Back"
        onPress={() => navigation.goBack()}
      />
      <Button
        title="Pop To Top"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}
function LogoTitle() {
  return (
    <Image
      style={{ width: 50, height: 50 }}
      source={{uri: 'https://cdn.freebiesupply.com/logos/thumbs/2x/git-logo.png'}}
    />
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home" 
          component={HomeScreen} 
          options={
            ({navigation}) => (
              {
                headerTitle: <LogoTitle />,
                headerStyle: {
                  backgroundColor: 'lightblue',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                },
                headerRight: () => (
                  <Icon 
                    name="add"
                    type="MaterialIcons"
                    style={{marginRight: 10, color: 'red'}}
                    onPress={() => navigation.navigate('Detail', {
                      id: 1,
                      name: 'Dara'
                    })}
                  />
            ),
              }
            )
          }
        />
        <Stack.Screen name="Detail" component={DetailScreen} options={{ title: 'លម្អិត' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;