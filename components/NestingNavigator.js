// Tab + Stack Navigator
import * as React from 'react';
import { View, Text, Button, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createStackNavigator} from '@react-navigation/stack'


// Feed Component
function Feed({navigation}) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Feed Screen</Text>
        <Button 
            title="GO to Profile"
            onPress={() => navigation.navigate('Profile')}
        />
      </View>
    );
  }
//   Message

function Messages({navigation}) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Message Screen</Text>
        
      </View>
    );
  }

//   profile

function Profile({navigation}) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Profile Screen</Text>
        
      </View>
    );
  }
//   Setting screen
function Setting({navigation}) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Setting Screen</Text>
        
      </View>
    );
}
const Tab = createBottomTabNavigator()
function Home() {
    return (
        <Tab.Navigator>
          <Tab.Screen name="Feed" component={Feed} />
          <Tab.Screen name="Messages" component={Messages} />
        </Tab.Navigator>
      )
  }


//   App
const Stack = createStackNavigator()
export function NestingNavigator() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="Setting" component={Setting} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }