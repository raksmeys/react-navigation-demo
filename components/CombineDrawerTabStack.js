/**
 * Drawer => invoke => Tab (MainStack & ContactStack)
 * 
 * MainStack (Home, About), Contact (Contact, Setting)
 * 
 * 
 */


import React from "react";
import { View, Button, Text, StyleSheet } from "react-native";
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Icon } from "native-base";

const styles = StyleSheet.create({
    center: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
    },

    hContainer: {
    flex: 1,
      justifyContent: "space-between",
      alignItems: "center",
      flexDirection: 'row', 
      margin: 10,
      flexWrap: 'wrap'

    }

  });

export const Home = ({navigation}) => {
  return (
    <View style={styles.hContainer}>
      <View>
          <Icon name="add" />
          <Text>Photo</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
          <Icon name="add" />
          <Icon name="add" />
          <Text>Photo</Text>
      </View>
      {/* <View>
          <Icon name="add" />
          <Text>Photo</Text>
      </View> */}
    </View>
  );
};

export const About = () => {
    return (
      <View style={styles.center}>
        <Text>This is the about screen</Text>
      </View>
    );
  };


export const Contact = ({navigation}) => {
    return (
      <View style={styles.center}>
        <Text>This is the contact screen</Text>
        <Button 
            title="Go to Setting"
            onPress={() => navigation.navigate('Setting')}
        />
      </View>
    );
  };
  const Setting = () => {
    return (
      <View style={styles.center}>
        <Text>This is the setting screen</Text>
      </View>
    );
  };


const Stack = createStackNavigator();
const MainStackNavigator = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="About" component={About} />
      </Stack.Navigator>
    );
  };


const ContactStackNavigator = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Contact" component={Contact} />
        <Stack.Screen name="Setting" component={Setting} />
      </Stack.Navigator>
    );
  };
export { MainStackNavigator, ContactStackNavigator };

const Tab = createBottomTabNavigator();
export const BottomTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={MainStackNavigator} />
      <Tab.Screen name="Contact" component={ContactStackNavigator} />
    </Tab.Navigator>
  );
};


const Drawer = createDrawerNavigator();
export const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={BottomTabNavigator} />
      <Drawer.Screen name="Contact" component={ContactStackNavigator} />
    </Drawer.Navigator>
  );
};



const CombinationNavigation = () => {
    return (
      <NavigationContainer>
        <DrawerNavigator />
      </NavigationContainer>
    );
  };
  export default CombinationNavigation;
