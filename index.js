/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import DrawerNavigation from './components/DrawerNavigation';
import IconSheet from './components/IconSheet';
import { NestingNavigator } from './components/NestingNavigator';
import CombinationNavigation from './components/CombineDrawerTabStack'

AppRegistry.registerComponent(appName, () => CombinationNavigation);
